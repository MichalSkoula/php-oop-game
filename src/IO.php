<?php 

class IO {

    public static function read() {
        echo '>';
        $fp = fopen('php://stdin', 'r');
        $line = fgets($fp, 1024);
        return trim($line);       
    }

    public static function writeLine(string $text = '') {
        usleep(50 * 1000);
        echo $text.PHP_EOL;
    }

    public static function write(string $text = '') {
        echo $text;
    }
    
}