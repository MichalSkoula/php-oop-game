<?php 

class Help {

    public static function manual() {
    	$commands = array(
			array('me', '', 'get informations about the player'),
			array('map', '', 'print map (also dumps player)'),
			array('move', '<direction> <steps>', 'movement, direction and optional number of steps'), 
			array('exit', '', 'exits game')
		);

        foreach($commands as $command) {
        	IO::writeLine(str_pad($command[0],6,' ').str_pad($command[1],25,' ').$command[2]);
        }
    }

}