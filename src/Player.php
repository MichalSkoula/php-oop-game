<?php 

class Player {

    private $name;
    private $health;
    private $x;
    private $y;

    public function __construct(string $name = 'John', int $x = 5, int $y = 5) {
        $this->name = $name;
        $this->health = 100;
        $this->x = $this->y = 5;
    }

    // locations
    public function getLocation(string $letter = '') {
        if($letter != '')
            return $this->{$letter};
        else
            return [$this->x, $this->y];
    }

    public function setLocation(int $x = 5, int $y = 5) {
        $this->x = (int)$x;
        $this->y = (int)$y;
    }

    // name
    public function setName(string $name) {
        $this->name = $name;
    }

    public function getName() {
        return $this->name;
    }

    // health
    public function setHealth(int $health) {
        $this->health = (int)$health;
    }

    // dump
    public function dump() {
        IO::writeLine($this->name.', health: '.$this->health.', at ['.$this->getLocation('x').','.$this->getLocation('y').']');
    }

    // movement
    public function move(string $direction, int $steps = 1, Map $map) {
        $steps = $steps ? $steps : 1;

        for($i = 0; $i < $steps; $i++) {
            $result = false;
            switch($direction) {
                case 'up':      if($map->isFree($this->x, $this->y - 1)) { $this->y--; $result = true; } break;
                case 'down':    if($map->isFree($this->x, $this->y + 1)) { $this->y++; $result = true; } break;
                case 'left':    if($map->isFree($this->x - 1, $this->y)) { $this->x--; $result = true; } break;
                case 'right':   if($map->isFree($this->x + 1, $this->y)) { $this->x++; $result = true; } break;
                default: break;
            }

            if($result) {
                IO::writeLine('Moving '.$direction.' by '.$steps.' step'); 
            } 
            else { 
                IO::writeLine('Sorry, cannot move there.');
            }
        }
    }

    // load from file
    public function load() {
        if(file_exists('save.json')) {
            $file = fopen('save.json', 'r');
            $content = json_decode(fread($file,filesize('save.json')));
            fclose($file); 

            if(isset($content->name)) {
                $this->setName($content->name);
            }

            if(isset($content->health)) {
                $this->setHealth($content->health);
            }

            if(isset($content->x) && isset($content->y)) {
                $this->setLocation($content->x, $content->y);
            }
        }
        else {
            IO::writeLine('Hello. What is your name? :)');
            $name = ucfirst(IO::read());
            $this->setName($name);
        }
    }

    // save to file
    public function save() {
        $object = new stdClass();
        $object->name = $this->name;
        $object->health = $this->health;
        $object->x = $this->x;
        $object->y = $this->y;
        file_put_contents('save.json', json_encode($object));
    }

}