<?php 

// load classes
require 'src/IO.php';
require 'src/Player.php';
require 'src/Map.php';
require 'src/Help.php';

// create map
$map = new Map();

// create/load player
$player = new Player();
$player->load();
$player->save();

// start
IO::writeLine('Hello '.$player->getName().'. You can play now. For help, type "help". Bye.');

// action!
while(true) {
    // save player
    IO::writeLine('');
    $player->save();
    
    // get command
    list($command, $argument1, $argument2) = array_pad(explode(' ', IO::read()), 3, '');

    //do it
    switch($command) {
        case 'help': Help::manual(); break;
        case 'me': $player->dump(); break;
        case 'map': $map->dump($player); break;
        case 'move': $player->move($argument1, (int)$argument2, $map); break;
        case 'exit': exit;
        default: IO::writeLine('What? Try again'); break;
    }
}